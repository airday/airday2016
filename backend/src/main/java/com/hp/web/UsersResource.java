package com.hp.web;

import com.hp.entity.User;
import com.hp.services.UsersService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;
import com.wordnik.swagger.annotations.ApiResponse;
import com.wordnik.swagger.annotations.ApiResponses;

import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Singleton
@Path("/users")
@Api(value = "Users")
public class UsersResource {

    private UsersService usersService = new UsersService();

    @POST
    @Path("/login")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Login")
    @ApiResponses(value = {
            @ApiResponse(code = 401, message = "Wrong username or password")
    })
    public Response login(@FormParam("username") String username, @FormParam("password") String password) {
        User user = usersService.login(username, password);

        if (user == null) {
            return Response.status(401).build();
        }
        return Response.status(200).entity(user).build();
    }

    @POST
    @Path("/register")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Register")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "All fields should be provided")
    })
    public Response register(@ApiParam(name = "User", value = "The new user", required = true) User user) {
        User addedUser = usersService.addUser(user);
        if (addedUser == null) {
            return Response.status(400).entity("Please provide all fields").build();
        }
        return Response.status(200).entity(addedUser).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get all users", response = User.class)
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "There are no users")
    })
    public Response getAllUsers() {
        List<User> users = usersService.getAllUsers();
        if (users.size() == 0) {
            return Response.status(204).build();
        }
        return Response.ok(users).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Get user by id", response = User.class)
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "There is no user with the given id")
    })
    public Response getUserById(@PathParam("id") int id) {
        User user = usersService.getUserById(id);
        if (user == null) {
            return Response.status(404).build();
        }
        return Response.ok(user).build();
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Update an existing user")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "There is no user with the given id")
    })
    public Response updateUser(@PathParam("id") int id,
                               @ApiParam(name = "User", value = "The updated user", required = true) User user) {
        if (!usersService.updateUser(id, user)) {
            return Response.status(404).build();
        }
        return Response.status(200).build();
    }

    @DELETE
    @Path("/{id}")
    @ApiOperation(value = "Remove an existing user")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "There is no user with the given id")
    })
    public Response removeUser(@PathParam("id") int id) {
        if (!usersService.deleteUser(id)) {
            return Response.status(404).build();
        }
        return Response.status(200).build();
    }
}
