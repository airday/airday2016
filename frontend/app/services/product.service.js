'use strict';

angular.module('codewars.services')
    .service('productService', [
        '$http',
        '$q',
        '$rootScope',
        'REST_BASE',
        function ($http, $q, $rootScope, REST_BASE) {

            var self = this;

            self.getProducts = function () {
                var defer = $q.defer();

                $http({
                    url: REST_BASE.PRODUCTS,
                    method: 'get'
                }).then(function (result) {
                        defer.resolve(result.data);
                    },
                    function (error) {
                        defer.reject(error);
                    });
                return defer.promise;
            };

            self.getImage = function (id) {
                var defer = $q.defer();

                $http({
                    url: REST_BASE.PRODUCTS + '/images/' + id,
                    method: 'get'
                }).then(function (result) {
                        defer.resolve(result.data);
                    },
                    function (error) {
                        defer.reject(error);
                    });
                return defer.promise;
            };

            self.deleteProduct = function (id) {
                var defer = $q.defer();

                $http({
                    url: REST_BASE.PRODUCTS + '/' + id,
                    method: 'delete',
                    headers: {'Content-Type': 'application/json'}
                }).then(function (result) {
                        defer.resolve(result.data);
                    },
                    function (error) {
                      defer.reject(error);
                    });
                return defer.promise;
            };

          self.updateProduct = function (product) {
              var defer = $q.defer();

            $http({
                url: REST_BASE.PRODUCTS + '/' + product.id,
                method: 'put',
                data: product,
                headers: {'Content-Type': 'application/json'}
            }).then(function (result) {
                    defer.resolve(result.data);
                },
                function (error) {
                    defer.reject(error);
                });
            return defer.promise;
          };

          self.addProduct = function (product) {
              var defer = $q.defer();
              var p = {
                  name: product.name,
                  provider: product.provider
              };
              $http({
                  url: REST_BASE.PRODUCTS,
                  method: 'post',
                  data: p,
                  headers: {'Content-Type': 'application/json'}
              }).then(function (result) {
                      defer.resolve(result.data);
                      $http({
                          url: REST_BASE.IMAGES + '/' + product.id,
                          method: 'post',
                          data: product.picture,
                          headers: {'Content-Type': undefined}
                      }).then(function (result) {
                              defer.resolve(result);
                          },
                          function(error) {
                              defer.reject(error);
                          });
                  },
                  function (error) {
                      defer.reject(error);
                  });

              return defer.promise;
          };

          self.addPicture = function (product) {
              var defer = $q.defer();

              $http({
                url: REST_BASE.IMAGES + '/' + product.id,
                method: 'post',
                data: product.picture,
                headers: {'Content-Type': undefined}
              }).then(function (result) {
                  defer.resolve(result);
                },
                function (error) {
                  defer.reject(error);
                });

              return defer.promise;
          };

        }]);
