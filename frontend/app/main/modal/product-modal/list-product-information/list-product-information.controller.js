'use strict';

/**
 * @ngdoc function
 * @name codewars2016App.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the codewars2016App
 */
angular.module('codewars2016App')
  .controller('ListProductInformationCtrl', ['$scope', 'productService', '$uibModalInstance', '$q', '$window', '$route', 'product',
    function ($scope, productService, $uibModalInstance, $q, $window, $route, product) {
      $scope.product = product;
      $scope.deleteProduct = function () {
          productService.deleteProduct(product.id).then(function () {
              $window.location.reload();
              $uibModalInstance.close();
          });
      };

      $scope.cancel = function () {
          $uibModalInstance.dismiss('cancel');
      };

      $scope.updateProduct = function() {
          productService.updateProduct(product).then(function () {
              $window.location.reload();
              $uibModalInstance.close();
          });
      };

    }]);
