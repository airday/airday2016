# Codewars 2016

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 0.11.1. Check the page in order to install the initial dependencies for the angular project.

* 1) Install git for windows
* 2) Go to the folder you want to download the source code. Then use the following command:
* git clone https://bitbucket.org/dinenicu/code-wars-2016.git
* 3) Install node.js for Windows. Use version 0.12.14:
* https://nodejs.org/download/release/v0.12.14/
* 4)Restart your PC
* 5)Go to your frontend-folder and run the following command:
* npm install --global yo bower grunt-cli
* 6)Install Ruby for windows
* http://rubyinstaller.org/
* 7)Install compass
* gem install compass
* 8)Install  sass-lint
* gem install scss-lint
* 9)Install MongoDB for windows. Install it as a service
* 10)Install .NET Framework 2.0
* https://www.microsoft.com/en-us/download/confirmation.aspx?id=15354
* 11)Also install the latest .Net Framework
* 12) Install Python  vs 2.7.11 (or latest 2.7 version)
* https://www.python.org/downloads/
* Add Python to path
* 13)Run the following commands: (Remember you must be in the frontend folder. This will take a while)
* npm install -dd
* bower i
* 14)To start the frontend application, start the backend server before, and after that run the following command:
* grunt serve

Note:
* If you can’t log into MongoDB, go to its installation folder and run mongod.exe
* If npm install fails because of reasons, just run it again. Then type grunt serve and check if the project builds.
* It is best to use IntelliJ IDEA as IDE
* https://www.jetbrains.com/idea/

## Build & development

Run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.


## Backend ##
### 1. Details ###

* the backend runs like a server application, listening on port 8081
* it exposes a REST webservice so that other applications can access it (e.g the frontend)

### 2. Prerequisites ###
You need the following installed and available in your PATH environment variable:

* Java 8 (http://www.oracle.com/technetwork/java/javase/downloads/index.html)
* Apache Maven 3.2.5 or greater (http://maven.apache.org/download.cgi)

### 3. Build and Run ###
Run the following commands in /backend folder:
```
#!shell
# Building
mvn clean package -Dmaven.test.skip=true

# Running
java -jar target/dependency/jetty-runner.jar --port 8081 target/*.war

# or you can use a single command
mvn jetty:run
```

For testing the backend functionality navigate to http://localhost:8081/ and you will see all the possible REST API calls.