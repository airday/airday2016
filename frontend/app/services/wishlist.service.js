'use strict';

angular.module('codewars.services')
    .service('wishlistService', [
        '$http',
        '$q',
        'REST_BASE',
        'notificationService',
        function ($http, $q, REST_BASE, notificationService) {

            var self = this;

            self.getUserWishlists = function (userID) {
                var defer = $q.defer();
                $http({
                    url: REST_BASE.WISHLISTS + '/' + userID,
                    method: 'GET',
                    headers: {'Content-Type': 'application/json'}
                }).then(function (result) {
                        defer.resolve(result.data);
                    },
                    function (error) {
                        defer.reject(error);
                    });
                return defer.promise;
            };

            self.addWishList = function (userID, wishlist) {
                var defer = $q.defer();
                $http({
                    url: REST_BASE.WISHLISTS + '/' + userID,
                    method: 'post',
                    data: wishlist,
                    headers: {'Content-Type': 'application/json'}
                }).then(function (result) {
                        defer.resolve(result.data);
                    },
                    function (error) {
                        defer.reject(error);
                    });
                return defer.promise;
            };

            self.updateWishList = function (userID, wishlist) {
                  console.log(wishlist);
                var defer = $q.defer();
                $http({
                    url: REST_BASE.WISHLISTS + '/' + userID,
                    method: 'put',
                    data: wishlist,
                    headers: {'Content-Type': 'application/json'}
                }).then(function (result) {
                        notificationService.notify('wishlistModified');
                        defer.resolve(result.data);
                    },
                    function (error) {
                        defer.reject(error);
                    });
                return defer.promise;
            };

            self.deleteWishList = function (userID, wishlistId) {
                var defer = $q.defer();
                $http({
                    url: REST_BASE.WISHLISTS + '/' + userID + '/' + wishlistId,
                    method: 'delete',
                    headers: {'Content-Type': 'application/json'}
                }).then(function (result) {
                        defer.resolve(result.data);
                    },
                    function (error) {
                        defer.reject(error);
                    });
                return defer.promise;
            };
        }]);
