'use strict';

/**
 * @ngdoc function
 * @name codewars2016App.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the codewars2016App
 */
angular.module('codewars2016App')
  .controller('RegisterCtrl', [
    '$scope', 'userService', '$location',
    function ($scope, userService, $location) {

      $scope.register = function () {
        userService.register($scope.user).then(function () {
          $location.path('/#login');
        });
      };

    }]);
