package com.hp.services;

import com.hp.entity.Wishlist;

import java.util.ArrayList;
import java.util.List;

public class WishlistService {
    private static WishlistService instance;
    private List<Wishlist> wishlist = new ArrayList<>();

    private WishlistService() {
    }

    public List<Wishlist> getAllPublicWishlist() {
        List<Wishlist> publicWishlists = new ArrayList<>();
        for (Wishlist wsl : wishlist) {
            if (wsl.isTypePublic()) {
                publicWishlists.add(wsl);
            }
        }
        return publicWishlists;
    }

    public static WishlistService getInstance() {
        if (instance == null) {
            instance = new WishlistService();
        }
        return instance;
    }
}
