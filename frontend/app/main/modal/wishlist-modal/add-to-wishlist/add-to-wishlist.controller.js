'use strict';

angular.module('codewars2016App')
    .controller('AddToWishlistCtrl', ['$scope', 'wishlistService', 'userService', '$q', '$uibModalInstance', 'product',
        function ($scope, wishlistService, userService, $q, $uibModalInstance, product) {
            var getWishlists;

            getWishlists = function () {
                wishlistService.getUserWishlists(userService.user.id).then(function (result) {
                   $scope.wishlists = result;
                }, function () {
                   $scope.wishlists = [];
                });
            };

            getWishlists();

            $scope.addToWishlist = function (wishlist) {
                if (!wishlist.products) {
                  wishlist.products = [];
                }
                var p = {
                  id: product.id,
                  name: product.name,
                  provider: product.provider
                };
                wishlist.products.push(p);
                wishlistService.updateWishList(userService.user.id, wishlist).then(function () {
                        $uibModalInstance.close();
                    });
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
    }]);
