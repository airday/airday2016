'use strict';

/**
 * @ngdoc function
 * @name codewars2016App.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the codewars2016App
 */
angular.module('codewars2016App')
  .controller('AddProductCtrl', ['$scope', 'productService', '$uibModalInstance', '$q', '$window',
    function ($scope, productService, $uibModalInstance, $q, $window) {

      $scope.addProduct = function () {
        productService.addProduct($scope.product).then(function () {
          $window.location.reload();
          $uibModalInstance.close();
        });
      };

      $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      };

    }]);
