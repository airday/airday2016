package com.hp.services;


import com.hp.entity.Product;
import com.hp.utils.Util;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.mongodb.MongoClient;
import java.net.UnknownHostException;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

public class ProductsService {
    private final static Logger LOGGER = Logger.getLogger(ProductsService.class.getName());
    private static ProductsService instance;

    private Random randomGenerator;
    private ImagesService imagesService;
    private Map<Integer, Product> productsById;

    private ProductsService() {
        imagesService = ImagesService.getInstance();
        productsById = new HashMap<>();
        randomGenerator = new Random();
        loadProducts();
    }

    public static ProductsService getInstance() {
        if (instance == null) {
            instance = new ProductsService();
        }
        return instance;
    }

    public List<Product> getAllProducts() {
        return new ArrayList<>(productsById.values());
    }

    public Product getProductById(int id) {
        return productsById.get(id);
    }

    public Product addProduct(Product product) {
        product.setId(productsById.size() + 1);
        productsById.put(product.getId(), product);
        saveProducts();
        loadProducts();
        return product;
    }

    public boolean updateProduct(int id, Product newProduct) {
        if (productsById.get(id) == null) {
            return false;
        }

        productsById.put(id, newProduct);
      saveProducts();
      loadProducts();
        return true;
    }

    public boolean removeProduct(int id) {
        if (productsById.get(id) == null) {
            return false;
        }
        try {
            imagesService.deleteImageById(id);
            productsById.remove(id);
        } catch (URISyntaxException | IOException e) {
            return false;
        }
      saveProducts();
      loadProducts();
        return true;
    }

    public Product searchProduct(String name) {
      for (Map.Entry<Integer, Product> entry : productsById.entrySet()) {
        if (entry.getValue().getName().equals(name)) {
          return entry.getValue();
        }
      }
      return null;
    }

  private void loadProducts() {
    try {
      MongoClient mongo = new MongoClient("localhost", 27017);
      DB db = mongo.getDB("hp");
      DBCollection table=db.getCollection("products");
      DBCursor cursor=table.find();
      productsById.clear();
      while (cursor.hasNext()) {
        DBObject obj=cursor.next();
        Product product=new Product();
        product.setName((String)obj.get("name"));
        product.setProvider((String)obj.get("provider"));
        product.setId(productsById.size());
        productsById.put(product.getId(),product);
      }
    } catch (UnknownHostException e) {
      LOGGER.log(Level.SEVERE, e.getMessage(), e);
    }
  }

  private void saveProducts() {
    try {
      MongoClient mongo = new MongoClient("localhost", 27017);
      DB db = mongo.getDB("hp");
      DBCollection oldtable = db.getCollection("products");
      oldtable.drop();
      DBCollection table = db.getCollection("products");
      for (Product product : productsById.values()) {
        BasicDBObject document = new BasicDBObject();
        document.put("name", product.getName());
        document.put("provider", product.getProvider());
        table.insert(document);
      }
    } catch (UnknownHostException e) {
      LOGGER.log(Level.SEVERE, e.getMessage(), e);
    }
  }
}
