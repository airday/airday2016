'use strict';

/**
 * @ngdoc overview
 * @name codewars2016App
 * @description
 * # codewars2016App
 *
 * Main module of the application.
 */

angular.module('codewars.constants',[]);
angular.module('codewars.services', []);
angular.module('codewars.directives', []);

angular
    .module('codewars2016App', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ui.router',
        'ngTouch',
        'ui.bootstrap',
        'codewars.constants',
        'codewars.services'
    ])
    .config(function ($routeProvider) {
        $routeProvider
            .when('/', {
                templateUrl: 'main/main.html',
                controller: 'MainCtrl'
            })
            .when('/login', {
                templateUrl: 'main/login/login.html',
                controller: 'LoginCtrl'
            })
            .when('/register', {
                templateUrl: 'main/register/register.html',
                controller: 'RegisterCtrl'
            })
            .when('/user', {
              templateUrl: 'main/user/user.html',
              controller: 'UserCtrl'
            })
            .when('/logout', {
                templateUrl: 'main/logout/logout.html',
                controller: 'LogoutCtrl'
            })
            .when('/product', {
                templateUrl: 'main/products/products.html',
                controller: 'ProductsCtrl'
            })
            .when('/wishlist', {
                templateUrl: 'main/wishlist/wishlist.html',
                controller: 'WishlistCtrl'
            })
            .otherwise({
                redirectTo: '/'
            });
    })
    .run(['$rootScope', '$location', '$cookies', '$http','userService',
        function ($rootScope, $location, $cookies, $http, userService) {
            // keep user logged in after page refresh
            $rootScope.globals = $cookies.getObject('globals');
            if (!!$rootScope.globals && $rootScope.globals.currentUser) {
                userService.user = $rootScope.globals.currentUser.advancedDetails;
                $http.defaults.headers.common.Authorization = 'Basic ' + $rootScope.globals.currentUser.authdata; // jshint ignore:line
            }

            $rootScope.$on('$locationChangeStart', function () {
                if ($location.path() !== '/login' && $location.path() !== '/' && $location.path() !== '/register' && (!$rootScope.globals || !$rootScope.globals.currentUser)) {
                    $location.path('/login');
                }
                if(($location.path() === '/login' || $location.path() ==='/') && (!!$rootScope.globals && !!$rootScope.globals.currentUser)) {
                    $location.path('/wishlist');
                }
            });
        }]);
