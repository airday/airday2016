package com.hp.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hp.entity.User;
import com.hp.entity.Wishlist;
import com.hp.utils.Util;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.mongodb.MongoClient;
import java.net.UnknownHostException;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;

public class UsersService {
    private final static Logger LOGGER = Logger.getLogger(UsersService.class.getName());

    private Map<Integer, User> usersById;
    private Map<Integer, Wishlist> wishlistsById;

    public UsersService() {
        wishlistsById = new HashMap<>();
        usersById = new HashMap<>();
            loadUsers();
    }

    public User login(String username, String password) {
        return getUserByUsernamePassword(username, password);
    }

    public boolean authenticate(String authCredentials) {
        if (null == authCredentials)
            return false;
        // header value format will be "Basic encodedstring" for Basic
        // authentication. Example "Basic YWRtaW46YWRtaW4="
        final String encodedUserPassword = authCredentials.replaceFirst("Basic" + " ", "");
        String usernameAndPassword;
        try {
            byte[] decodedBytes = Base64.getDecoder().decode(
                    encodedUserPassword);
            usernameAndPassword = new String(decodedBytes, "UTF-8");
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
            return false;
        }
        final StringTokenizer tokenizer = new StringTokenizer(
                usernameAndPassword, ":");
        final String username = tokenizer.nextToken();
        final String password = tokenizer.nextToken();

        return getUserByUsernamePassword(username, password) != null;
    }

    public User addUser(User user) {
        if (!user.getFirstName().trim().isEmpty() && !user.getLastName().trim().isEmpty() &&
                !user.getEmail().trim().isEmpty() && !user.getUsername().trim().isEmpty() &&
                !user.getPassword().trim().isEmpty()) {
            user.setId(usersById.size());
            user.setPassword(Util.encryptUsingMD5(user.getPassword()));
            usersById.put(user.getId(), user);
            saveUsers();
            loadUsers();
            return user;
        }
        return null;
    }

    public boolean updateUser(int id, User updatedUser) {
        if (usersById.get(id) == null) {
            return false;
        }
        usersById.put(id, updatedUser);
      saveUsers();
      loadUsers();
        return true;
    }

    public boolean deleteUser(int id) {
        if (usersById.get(id) == null) {
            return false;
        }
        usersById.remove(id);
      saveUsers();
      loadUsers();
        return true;
    }

    public User getUserById(int id) {
        return usersById.get(id);
    }

    public List<User> getAllUsers() {
        return new ArrayList<>(usersById.values());
    }

    private User getUserByUsernamePassword(String username, String password) {
        for (Map.Entry<Integer, User> entry : usersById.entrySet()) {
            if (entry.getValue().getUsername().equals(username) &&
                    entry.getValue().getPassword().equals(Util.encryptUsingMD5(password))) {
                return entry.getValue();
            }
        }
        return null;
    }

    public List<Wishlist> getUserWishlists(int userId) {
        User user = usersById.get(userId);

        if (user != null) {
            return user.getWishlists();
        }
        return null;
    }

    public Wishlist addUserWishlist(int userId, Wishlist wishlist) {
        User user = usersById.get(userId);

        wishlist.setId(wishlistsById.size() + 1);
        user.getWishlists().add(wishlist);
        wishlistsById.put(wishlist.getId(), wishlist);
      saveUsers();
      loadUsers();
        return wishlist;
    }

    public boolean removeUserWishlist(int userId, int wishlistId) {
        User user = usersById.get(userId);

        for (Wishlist wishlist : user.getWishlists()) {
            if (wishlist.getId() == wishlistId) {
                user.getWishlists().remove(wishlist);
                wishlistsById.remove(wishlistId);
              saveUsers();
              loadUsers();
                return true;
            }
        }
        return false;
    }

    public void updateUserWishlist(int userId, Wishlist newWishlist) {
        User user = usersById.get(userId);

        for (Wishlist wishlist : user.getWishlists()) {
            if (wishlist.getId() == newWishlist.getId()) {
                wishlist.setName(newWishlist.getName());
                wishlist.setProducts(newWishlist.getProducts());
            }
        }
      saveUsers();
      loadUsers();
    }

    private void loadUsers() {
      try {
        MongoClient mongo = new MongoClient("localhost", 27017);
        DB db = mongo.getDB("hp");
        DBCollection table=db.getCollection("users");
        DBCursor cursor=table.find();
        usersById.clear();
        while (cursor.hasNext()) {
          DBObject obj=cursor.next();
          User user=new User();
          user.setUsername((String)obj.get("username"));
          user.setPassword((String)obj.get("password"));
          user.setFirstName((String)obj.get("firstName"));
          user.setLastName((String)obj.get("lastName"));
          user.setEmail((String)obj.get("email"));
          user.setId(usersById.size());
          usersById.put(user.getId(),user);
        }
      } catch (UnknownHostException e) {
          LOGGER.log(Level.SEVERE, e.getMessage(), e);
      }
    }

    private void saveUsers() {
        try {
            MongoClient mongo = new MongoClient("localhost", 27017);
            DB db = mongo.getDB("hp");
            DBCollection oldtable = db.getCollection("users");
            oldtable.drop();
            DBCollection table = db.getCollection("users");
            for (User user : usersById.values()) {
              BasicDBObject document = new BasicDBObject();
              document.put("username", user.getUsername());
              document.put("password", user.getPassword());
              document.put("firstName", user.getFirstName());
              document.put("lastName", user.getLastName());
              document.put("email", user.getEmail());
              table.insert(document);
            }
        } catch (UnknownHostException e) {
            LOGGER.log(Level.SEVERE, e.getMessage(), e);
        }
    }
}
