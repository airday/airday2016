package com.hp;

import com.hp.utils.AuthenticationFeature;
import com.hp.utils.AuthenticationFilter;
import com.hp.utils.CORSResponseFilter;
import com.wordnik.swagger.jaxrs.config.BeanConfig;
import org.glassfish.jersey.filter.LoggingFilter;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;

import javax.ws.rs.ApplicationPath;

@ApplicationPath("/services/api")
public class ServicesConfig extends ResourceConfig {

    public ServicesConfig() {
        // integration with Swagger
        BeanConfig beanConfig = new BeanConfig();
        beanConfig.setBasePath("/services/api");
        beanConfig.setResourcePackage("com.hp.web");

        beanConfig.setTitle("Online MyRegistry API");
        beanConfig.setDescription("Use the Online Services API for backend operations");
        beanConfig.setVersion("1.0");

        beanConfig.setLicense("Apache License, Version 2.0");
        beanConfig.setLicenseUrl("http://www.apache.org/licenses/LICENSE-2.0.html");
        beanConfig.setScan(true);

        packages("com.wordnik.swagger.jaxrs.json");
        packages("com.wordnik.swagger.jersey.listing");

        // register our resources
        packages("com.hp.web");

        register(CORSResponseFilter.class);

        // register for authentication filter configuration
        register(AuthenticationFeature.class);

        // register Jackson for custom deserializers
        register(JacksonFeature.class);

        // register the logging
        register(LoggingFilter.class);

        // register for multipart support
        register(MultiPartFeature.class);

        // enable tracing
        property(ServerProperties.TRACING, "ALL");
        property(ServerProperties.TRACING_THRESHOLD, "TRACE");
    }
}
