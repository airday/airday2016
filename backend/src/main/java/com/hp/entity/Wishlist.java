package com.hp.entity;

import java.util.List;

public class Wishlist {

    private int id;
    private String name;
    private boolean typePublic;
    private List<Product> products;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isTypePublic() {
        return typePublic;
    }

    public void setTypePublic(boolean typePublic) {
        this.typePublic = typePublic;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public String toString() {
        return "{name:'"+name+"',typePublic:'"+typePublic+"',products:"+products.toString()+"}";
    }
}
