'use strict';

angular.module('codewars.constants', [])
    .constant('REST_BASE', (function () {
        var base = 'http://localhost:8081/services/api';
        return {
            USERS: base + '/users',
            PRODUCTS: base + '/products',
            IMAGES: base + '/images',
            WISHLISTS: base+ '/wishlists'
        };
    })());

