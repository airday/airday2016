'use strict';

angular.module('codewars2016App')
  .controller('LogoutCtrl', ['$scope' , '$cookies', 'userService', '$location' , function ($scope, $cookies, userService, $location) {
    userService.logout();
    $location.path('/');
  }]);
